import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import { updateSchool, deleteSchool } from '../actions/schoolList'
import Form from '../components/Form'

class SchoolCreate extends Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
    this.delete = this.delete.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      this.props.activeSchool.name,
      () => this.props.history.goBack()
    )
  }

  submit(args) {
    this.props.updateSchool(this.props.activeSchool.id, args.name)
    this.props.history.push('/school_list')
  }

  delete() {
    this.props.deleteSchool(this.props.activeSchool.id)
    this.props.history.push('/school_list')
  }

  render() {
    const inputs = [
      {
        name: 'Nome',
        input: {
          name: 'name',
          type: 'text',
        },
      }
    ]

    const state = {
      name: '',
    }

    return (
      <section className="form-section">
        <Form
          title="Editar Escola"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          action="Editar"
          deleteHandler={this.delete}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    updateSchool,
    deleteSchool,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeSchool: state.activeSchool
})

export default connect(mapStateToProps, mapDispatchToProps)(SchoolCreate)

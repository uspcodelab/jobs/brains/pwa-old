import { SET_HEADER_HEIGHT } from '../actions/setHeader'

export default (state = 0, action) => {
  switch(action.type) {
    case SET_HEADER_HEIGHT:
      return action.height

    default:
      return state
  }
}

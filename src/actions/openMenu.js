export const SET_MENU = 'SET_MENU'

export const openMenu = () => dispatch => {
  document.querySelector('#overlay').addEventListener('click', () => {
    document.querySelector('#overlay').removeEventListener('click', () => {
      dispatch({
        type: SET_MENU,
        menuState: false
      })
    })

    dispatch({
      type: SET_MENU,
      menuState: false
    })
  })

  dispatch ({
    type: SET_MENU,
    menuState: true
  })
}

export const closeMenu = () => ({
  type: SET_MENU,
  menuState: false
})

export const toggleMenu = state => ({
  type: SET_MENU,
  menuState: !state
})

import React, { Component } from 'react'
import { MdAdd } from 'react-icons/lib/md'
import { FaCaretRight, FaCaretDown } from 'react-icons/lib/fa'

import TaskItem from './TaskItem'
import NewTaskModal from './NewTaskModal'

class TaskList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      taskFocused: null,
      newTaskModal: false,
    }

    this.handleFocusTask = this.handleFocusTask.bind(this)
    this.handleUnfocusTask = this.handleUnfocusTask.bind(this)
    this.toggleAccordion = this.toggleAccordion.bind(this)
    this.handleEndAddTask = this.handleEndAddTask.bind(this)
    this.handleCancelAddTask = this.handleCancelAddTask.bind(this)
    this.handleOpenNewTaskModal = this.handleOpenNewTaskModal.bind(this)
    this.sortTasks = this.sortTasks.bind(this)

    this.sortTasks()
  }

  sortTasks() {
    this.props.sortTasks(this.props.tasks)
  }

  handleFocusTask(obj) {
    this.setState({ taskFocused: obj.id })
  }

  handleUnfocusTask(obj) {
    this.setState({ taskFocused: null })
  }

  handleEndAddTask(newTask) {
    this.props.tasks.unshift(newTask)
    this.setState({ newTaskModal: false })
  }

  handleCancelAddTask() {
    this.setState({ newTaskModal: false })
  }

  handleOpenNewTaskModal() {
    this.setState({ newTaskModal: true })
  }

  toggleAccordion(ev) {
    this.setState({ isOpen: !this.state.isOpen })
  }

  getNewTaskModal() {
    if (this.state.newTaskModal) {
      return (
        <NewTaskModal
          projectId={this.props.projectId}
          handleCancelAddTask={this.handleCancelAddTask}
          handleEndAddTask={this.handleEndAddTask}
        />
      )
    }
  }

  getCaretIcon() {
    if (this.state.isOpen) {
      return <FaCaretDown />
    } else {
      return <FaCaretRight />
    }
  }

  getAddButton() {
    if (this.props.addAble) {
      return (
        <MdAdd
          className="task-list-add-button"
          onClick={this.handleOpenNewTaskModal}
        />
      )
    }
  }

  getAccordionTitle() {
    return this.props.title
  }

  getPanelObject() {
    const panelClasses =
      'task-list-panel' + (!this.state.isOpen ? ' task-list-panel-closed' : '')
    let taskItems = null

    const createTaskItem = (task, isFocused) => {
      return (
        <TaskItem
          key={task.id}
          task={task}
          handleArchive={this.props.handleArchive}
          handleDelete={this.props.handleDelete}
          handleEdit={this.props.handleEdit}
          handleFocusTask={this.handleFocusTask}
          handleUnfocusTask={this.handleUnfocusTask}
          isTaskFocused={isFocused}
          isArchived={this.props.isArchived}
        />
      )
    }

    if (this.state.taskFocused == null) {
      taskItems = this.props.tasks.map(task => {
        return createTaskItem(task, false)
      })
    } else {
      taskItems = this.props.tasks.reduce((res, task) => {
        if (task.id === this.state.taskFocused) {
          res.push(createTaskItem(task, true))
        }
        return res
      }, [])
    }

    return (
      <div className={panelClasses}>
        {this.getNewTaskModal()}
        <div className="task-list-panel-space" />
        {taskItems}
        <div className="task-list-panel-space" />
      </div>
    )
  }

  getAccordionObject() {
    const accordionTitleClasses =
      'task-list-accordion-title' +
      (this.state.isOpen
        ? ' task-list-accordion-open'
        : ' task-list-accordion-closed')
    return (
      <div className="task-list-accordion">
        <div className={accordionTitleClasses} onClick={this.toggleAccordion}>
          {this.getCaretIcon()}
          {this.getAccordionTitle()}
        </div>
        {this.getAddButton()}
      </div>
    )
  }

  render() {
    return (
      <div className="task-list">
        {this.getAccordionObject()}
        {this.getPanelObject()}
      </div>
    )
  }
}

export default TaskList

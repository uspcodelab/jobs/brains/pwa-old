import { SELECT_CLASSROOM, CLEAN_CLASSROOM } from '../actions/classroomList'

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_CLASSROOM:
      return action.classroom

    case CLEAN_CLASSROOM:
      return action.payload

    default:
      return state
  }
}

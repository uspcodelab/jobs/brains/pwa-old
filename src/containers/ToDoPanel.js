import React, { Component } from 'react'

import TaskList from '../components/TaskComponents/TaskList'

class ToDoPanel extends Component {
  constructor(props) {
    super(props)

    this.toggleArchived = this.toggleArchived.bind(this)
    this.handleDelete = this.handleDelete.bind(this)

    this.allTaskList = this.props.tasks
    this.state = {
      tasks: this.getNotArchivedTasks(),
      archivedTasks: this.getArchivedTasks(),
    }
  }

  buildState() {
    this.setState({
      tasks: this.getNotArchivedTasks(),
      archivedTasks: this.getArchivedTasks(),
    })
  }

  getArchivedTasks() {
    return this.allTaskList.filter(task => task.archived === true)
  }

  getNotArchivedTasks(allTaskList) {
    return this.allTaskList.filter(task => task.archived === false)
  }

  sortFirstDeadline(tasks) {
    tasks.sort((task1, task2) => {
      if (task1.done === task2.done) {
        if (task1.deadline === task2.deadline) {
          return 0
        } else if (task1.deadline == null) {
          return -1
        } else if (task2.deadline == null) {
          return 1
        } else return task1.deadline.getTime() - task2.deadline.getTime()
      } else return task1.done ? 1 : -1
    })
  }

  toggleArchived(task) {
    this.allTaskList.map(obj => {
      if (obj.id === task.id) {
        obj.archived = !obj.archived
      }
      return obj
    })
    this.buildState()
  }

  handleDelete(task) {
    this.allTaskList = this.allTaskList.filter(obj => obj.id !== task.id)
    this.buildState()
  }

  render() {
    return (
      <div>
        <TaskList
          projectId={this.props.projectId}
          title="Tarefas"
          tasks={this.state.tasks}
          handleArchive={this.toggleArchived}
          handleDelete={this.handleDelete}
          isArchived={false}
          addAble={true}
          sortTasks={this.sortFirstDeadline}
        />
        <TaskList
          projectId={this.props.projectId}
          title="Arquivo"
          tasks={this.state.archivedTasks}
          handleArchive={this.toggleArchived}
          handleDelete={this.handleDelete}
          isArchived={true}
          addAble={false}
          sortTasks={this.sortFirstDeadline}
        />
      </div>
    )
  }
}

export default ToDoPanel

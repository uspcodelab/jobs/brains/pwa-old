import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { selectProfile } from '../actions/profileManagement'

class DropMenu extends Component {
  constructor(props) {
    super(props)

    this.showMenu = this.showMenu.bind(this)
    this.closeMenu = this.closeMenu.bind(this)
    this.renderMenu = this.renderMenu.bind(this)
    this.renderButtons = this.renderButtons.bind(this)
    this.handleClick = this.handleClick.bind(this)

    this.state = {
      showMenu: false,
      profiles: []
    }
  }

  async componentDidMount() {
    const getAllProfiles = gql`
      query getAllProfilesFunc {
        getAllProfiles {
          name
        }
      }
    `

    const response = await client.query({
      query: getAllProfiles
    })

    const profiles = response.data.getAllProfiles.map(profile => {
      return profile.name
    })

    this.setState({ profiles })
  }

  showMenu(event) {
    event.preventDefault()

    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu)
    })
  }

  closeMenu() {
    this.setState({ showMenu: false }, () => {
      document.removeEventListener('click', this.closeMenu);
    });
  }

  handleClick(event) {
    this.props.selectProfile(event.target.name)
  }

  renderButtons() {
    return this.state.profiles.map(profile => {
      return (
        <button
          onClick={ this.handleClick }
          name={ profile }
          key={ profile }
        >
          { profile }
        </button>
      )
    })
  }

  renderMenu() {
    if (!this.state.showMenu)
      return (
        <button onClick={this.showMenu}>
          {
            !this.props.selectedProfile
            ? 'Show Menu'
            : this.props.selectedProfile
          }
        </button>
      )

    return (
      <div>
        { this.renderButtons() }
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderMenu()}
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    selectProfile
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = (state) => {
  return {
    selectedProfile: state.selectedProfile
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DropMenu)

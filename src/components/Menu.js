import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { setHeader } from '../actions/setHeader'

class Menu extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.setHeader(
      'Menu',
      () => this.props.history.push('/')
    )
  }

  render() {
    return (
      <div className="page">

        <div
          className="btn btn-yellow btn-link"
          onClick={() => this.props.history.push('/school_list')}
        >
          <div>Escolas</div>
        </div>

        <div
          className="btn btn-yellow btn-link"
          onClick={() => this.props.history.push('/profile_management')}
        >
          <div>Gerencimento de Pefis </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

export default connect(null, mapDispatchToProps)(Menu)

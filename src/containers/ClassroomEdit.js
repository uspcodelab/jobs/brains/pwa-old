import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import { updateClassroom, deleteClassroom } from '../actions/classroomList'
import Form from '../components/Form'

class SchoolCreate extends Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
    this.delete = this.delete.bind(this)
  }

  componentDidMount() {
    const { year, complement, period } = this.props.activeClass
    const title = `${year}º ${complement} ${period}`
    this.props.setHeader(
      title,
      () => this.props.history.goBack()
    )
  }

  submit(args) {
    args.year = args.year? args.year : this.props.activeClass.year
    args.complement = args.complement? args.complement : this.props.activeClass.complement
    args.period = args.period? args.period : this.props.activeClass.period

    this.props.updateClassroom(this.props.activeClass.id, args)
    this.props.history.push('/classroom_list')
  }

  delete() {
    this.props.deleteClassroom(this.props.activeClass.id)
    this.props.history.push('/classroom_list')
  }

  render() {
    const inputs = [
      {
        name: 'Ano',
        input: {
          name: 'year',
          type: 'text',
        },
      },
      {
        name: 'Complemento',
        input: {
          name: 'complement',
          type: 'text',
        },
      },
      {
        name: 'Período',
        input: {
          name: 'period',
          type: 'text',
        },
      }
    ]

    const state = {
      year: '',
      complement: '',
      period: ''
    }

    return (
      <section className="form-section">
        <Form
          title="Editar Turma"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          action="Editar"
          deleteHandler={this.delete}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    updateClassroom,
    deleteClassroom,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeClass: state.activeClass
})

export default connect(mapStateToProps, mapDispatchToProps)(SchoolCreate)

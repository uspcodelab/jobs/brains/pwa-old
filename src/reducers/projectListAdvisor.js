import { FETCH_PROJECTS_ADVISOR } from '../actions/projectList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_PROJECTS_ADVISOR:
      return action.projectList

    default:
      return state
  }
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FaPencil } from 'react-icons/lib/fa'

import List from '../components/List'
import { setHeader } from '../actions/setHeader'
import { fetchClassrooms, selectClassroom } from '../actions/classroomList'

class ClassroomList extends Component {
  constructor(props) {
    super(props)

    this.itemRender = this.itemRender.bind(this)
    this.classroomClick = this.classroomClick.bind(this)
  }

  componentDidMount() {
    if (!this.props.classroomList.length)
      this.props.fetchClassrooms(this.props.activeSchool.id)
    this.props.setHeader(
      'Turmas',
      () => this.props.history.goBack(),
      () => this.props.history.push('/classroom_new')
    )
  }

  classroomClick(obj) {
    this.props.history.push('/project_list')
    this.props.selectClassroom(obj)
  }

  classroomString(classroom) {
    return `${classroom.year}º ${classroom.complement} - ${classroom.period}`
  }

  classroomEdit(obj, ev) {
    ev.stopPropagation()
    this.props.selectClassroom(obj)
    this.props.history.push('/classroom_edit')
  }

  itemRender(obj) {
    return (
      <div
        key={obj.id}
        onClick={e => this.classroomClick(obj)}
        className="card"
      >
        <div className="card-title"> {this.classroomString(obj)} </div>
        <FaPencil onClick={ev => this.classroomEdit(obj, ev)} />
      </div>
    )
  }

  render() {
    return (
      <section className="page">
        <List
          objList={this.props.classroomList}
          itemRender={this.itemRender}
          buttonLink={'classrooms_new'}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader,
    fetchClassrooms,
    selectClassroom,
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    classroomList: state.classroomList,
    activeSchool: state.activeSchool,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClassroomList)

import React, { Component } from 'react'
import Calendar from 'react-calendar'

import Modal from '../Modal'

class TaskCalendarModal extends Component {
  constructor(props) {
    super(props)

    this.onChangeCalendar = this.onChangeCalendar.bind(this)
    this.saveDate = this.saveDate.bind(this)
    this.cancelChange = this.cancelChange.bind(this)

    this.state = {
      deadline: this.props.deadline,
    }
  }

  saveDate() {
    this.props.closeModal(this.state.deadline)
  }

  cancelChange() {
    this.props.closeModal(null)
  }

  onChangeCalendar(date) {
    this.setState({ deadline: date })
  }

  getInsideModalRender() {
    return (
      <div className="task-calendar-modal-container">
        <Calendar
          onChange={this.onChangeCalendar}
          value={this.state.deadline}
        />
        <div className="task-calendar-modal-buttons-container">
          <div className="btn btn-blue" onClick={this.saveDate}>
            {' '}
            Salvar{' '}
          </div>
          <div className="btn" onClick={this.cancelChange}>
            {' '}
            Cancelar{' '}
          </div>
        </div>
      </div>
    )
  }

  render() {
    return <Modal render={this.getInsideModalRender()} />
  }
}

export default TaskCalendarModal

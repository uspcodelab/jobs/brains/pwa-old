import { SET_PROFILES, CLEAN_PROFILES } from '../actions/setProfiles'

export default (state = null, action) => {
  switch(action.type) {
    case SET_PROFILES:
      return action.profiles

    case CLEAN_PROFILES:
      return action.payload

    default:
      return state
  }
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import gql from 'graphql-tag'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import { newProject } from '../actions/projectList'

const TOTAL_PAGES = 8

class ProjectCreate extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      students: [{title:'Estudante 1', name:'student1'}, {title:'Estudante 2', name:'student2'}],
      student1: '',
      student2: '',
      advisors: [{title:'Orientador 1', name:'advisor1'}, {title:'Orientador 2', name:'advisor2'}],
      page: 0
    }

    this.submit = this.submit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.renderFormInput = this.renderFormInput.bind(this)
  }

  componentDidMount() {
    let title;
    if (this.props.activeClass) {
      const { year, complement, period } = this.props.activeClass
      title = `${year}º ${complement} ${period}`
    } else {
      title = `Criar projeto`
    }

    this.props.setHeader(
      title,
      () => this.props.history.goBack()
    )
  }

  async submit(args) {
    this.props.newProject(this.state)
    this.props.history.push('/project_list')
  }

  handleChange(event) {
    this.setState({ [`${event.target.name}`]: event.target.value })
  }

  renderInputs(title, name, className) {
    return (
      <div className="form-project-item" key={ name }>
        <div className="form-project-item-header">
          { title }
        </div>
        <textarea
          className={ className }
          name={ name }
          type='text'
          value={ this.state[`${ name }`] }
          onChange={ this.handleChange }
        />
      </div>
      )
  }

  renderFormInput() {
    let inputs = []
    let className = 'form-project-small-input'
    
    return (
      <div>
        <div className="form-project-item" key='name'>
          <div className="form-project-item-header">
            Nome do Projeto
          </div>
          <textarea
            className={ className }
            name='name'
            type='text'
            value={ this.state['name'] }
            onChange={ this.handleChange }
          />
        </div>
        {
          this.state.students.map(({title, name}) => {
            return this.renderInputs(title, name, className)
          })
        }
        <button
          className='btn'
          onClick={
            () => {
              let length = this.state.students.length + 1
              this.setState({students: [...this.state.students, {'name': 'student' + length, 'title':'Estudante ' + length}]})
            }
          } >
          + estudante
        </button>
        {
          this.state.advisors.map(({title, name}) => {
            return this.renderInputs(title, name, className)
          })
        }
      </div>
    )
  }

  render() {

    return (
      <div>

        <section className="form-project-container" id="projectContainer">
          { this.renderFormInput() }
          <div className="btn-section">
            <button
              className="btn"
              onClick={ this.submit }>
              Salvar
            </button>
          </div>
        </section>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader,
    newProject
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeClass: state.activeClass,
})

export default connect(mapStateToProps, mapDispatchToProps)(ProjectCreate)

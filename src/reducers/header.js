import { SET_HEADER, SET_HEADER_HEIGHT } from '../actions/setHeader'

const defaultValue = {
  title: 'Brains',
  goBackCallback: null,
  addCallback: null
}

export default (state = defaultValue, action) => {
  switch(action.type) {
    case SET_HEADER:
      return action.payload

    default:
      return state
  }
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import List from '../components/List'
import { setHeader } from '../actions/setHeader'
import { fetchProjects, selectProject } from '../actions/projectList'

class ProjectList extends Component {
  constructor(props) {
    super(props)

    this.itemRender = this.itemRender.bind(this)
    this.projectClick = this.projectClick.bind(this)
  }

  componentDidMount() {
    if (this.props.activeClass)
      this.props.fetchProjects(
        this.props.activeProfiles,
        this.props.activeClass.id
      )
    else
      this.props.fetchProjects(this.props.activeProfiles)

    let title, link;

    if (this.props.activeClass) {
      const { year, complement, period } = this.props.activeClass
      title = `${year}º ${complement} ${period}`
      link = '/class_menu'
    } else {
      title = `Projetos`
      link = '/project_new'
    }

    this.props.setHeader(
      title,
      () => this.props.history.goBack(),
      () => this.props.history.push(link)
    )
  }

  projectClick(obj) {
    this.props.history.push('/project')
    this.props.selectProject(obj)
  }

  itemRender(obj) {
    return (
      <div key={obj.id} onClick={e => this.projectClick(obj)} className="card">
        <div className="card-title"> {obj.name} </div>
      </div>
    )
  }

  render() {

    return (
      <section className="page">
        <List
          objList={this.props.projectList}
          itemRender={this.itemRender}
          buttonLink={'projects_new'}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    fetchProjects,
    selectProject,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    activeClass: state.activeClass,
    activeProfiles: state.activeProfiles,
    projectList: state.projectList,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectList)

  import React, { Component } from 'react'
  import { connect } from 'react-redux'
  import { bindActionCreators } from 'redux'
  import { FaBars, FaArrowLeft } from 'react-icons/lib/fa'

  import { toggleMenu } from '../actions/openMenu'
  import { setHeaderHeight } from '../actions/setHeader'

  class Header extends Component {
    constructor(props) {
      super(props)
      this.toggleMenu = this.toggleMenu.bind(this)
    }

    componentDidMount() {
    const height = document.querySelector('.header').clientHeight
    this.props.setHeaderHeight(height)
  }

  renderBackIcon(callback) {
    if (callback && this.props.pageHistory.location.pathname !== this.props.landingLoginPage) {
      return (
        <FaArrowLeft
          className="header-icon header-left-icon"
          onClick={callback}
        />
      )
    }
    return ''
  }

  toggleMenu() {
    this.props.toggleMenu(this.props.menu)
  }

  render() {
    const { title, goBackCallback, addCallback } = this.props.header

    return (
      <header className="header">
        { this.renderBackIcon(goBackCallback) }
        <div className="header-title">{title}</div>
        <FaBars
          className="header-icon header-right-icon"
          onClick={ this.toggleMenu }
        />
      </header>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    toggleMenu,
    setHeaderHeight
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    menu: state.menu,
    header: state.header,
    landingLoginPage: state.landingLoginPage
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)

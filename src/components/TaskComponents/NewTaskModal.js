import React, { Component } from 'react'
import { MdClose } from 'react-icons/lib/md'
import { FaBars, FaCalendarO } from 'react-icons/lib/fa'
import AutoResizeTextArea from 'react-textarea-autosize'
import gql from 'graphql-tag'

import { client } from '../../index'

import Modal from '../Modal'
import TaskCalendarModal from './TaskCalendarModal'

function getNewTask() {
  return {
    title: '',
    description: '',
    deadline: null,
    done: false,
    archived: false,
  }
}

function DateToDateTimeString(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}

async function submitNewTask(task, projectId, callback) {
  const newTask = gql`
    mutation newTask(
      $projectId: ID!
      $title: String!
      $description: String!
      $deadline: DateTime
      $done: Boolean!
      $archived: Boolean!
    ) {
      createTask(
        projectId: $projectId
        title: $title
        description: $description
        deadline: $deadline
        done: $done
        archived: $archived
      ) {
        id
        title
        description
        deadline
        done
        archived
      }
    }
  `

  const response = await client.mutate({
    mutation: newTask,
    variables: {
      projectId: projectId,
      title: task.title,
      description: task.description,
      deadline:
        task.deadline == null ? null : DateToDateTimeString(task.deadline),
      done: task.done,
      archived: task.archived,
    },
  })
  console.log('Create', response)
  let ret = { ...response.data.createTask }
  if (ret.deadline != null) ret.deadline = new Date(ret.deadline.slice(0, -1))
  console.log('New task', ret)
  callback(ret)
}

class NewTaskModal extends Component {
  constructor(props) {
    super(props)

    this.task = getNewTask()
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.handleChangeTitleText = this.handleChangeTitleText.bind(this)
    this.handleChangeDescriptionText = this.handleChangeDescriptionText.bind(
      this
    )
    this.getInsideModalRender = this.getInsideModalRender.bind(this)
    this.handleDeleteDeadline = this.handleDeleteDeadline.bind(this)
    this.handleSaveTask = this.handleSaveTask.bind(this)

    this.state = {
      title: this.task.title,
      description: this.task.description,
      calendarModalOpen: false,
    }
  }

  handleChangeTitleText(ev) {
    this.setState({ title: ev.target.value })
  }

  handleChangeDescriptionText(ev) {
    this.setState({ description: ev.target.value })
  }

  handleDeleteDeadline(ev) {
    ev.stopPropagation()
    this.handleChangeDate(null, true)
  }

  handleOpenModal() {
    this.setState({ calendarModalOpen: true })
  }

  handleChangeDate(newDate, acceptNull) {
    if (newDate != null || acceptNull !== false) {
      this.task.deadline = newDate
    }
    this.setState({ calendarModalOpen: false })
  }

  handleSaveTask() {
    this.task.title = this.state.title
    this.task.description = this.state.description
    if (this.task.title.length === 0) {
      this.props.handleCancelAddTask()
    } else {
      submitNewTask(
        this.task,
        this.props.projectId,
        this.props.handleEndAddTask
      )
    }
  }

  getDateDiv() {
    const text = this.task.deadline ? this.task.deadline.ddmm() : 'Definir data'
    const dateClasses = ['new-task-date-container']
    if (this.task.deadline) {
      dateClasses.push('new-task-date-valid')
    }
    return (
      <div className="new-task-date" onClick={this.handleOpenModal}>
        <FaCalendarO className="new-task-icons" />
        <div className={dateClasses.join(' ')}>
          {text}
          {this.task.deadline ? (
            <MdClose
              className="new-task-date-close-icon"
              onClick={this.handleDeleteDeadline}
            />
          ) : (
            ''
          )}
        </div>
      </div>
    )
  }

  getCalendarModal() {
    if (this.state.calendarModalOpen) {
      return (
        <TaskCalendarModal
          deadline={this.task.deadline}
          closeModal={obj => this.handleChangeDate(obj, false)}
        />
      )
    }
  }

  getInsideModalRender() {
    return (
      <div className="new-task-container">
        {this.getCalendarModal()}

        <AutoResizeTextArea
          className="new-task-textarea new-task-title"
          minRows={1}
          maxRows={3}
          value={this.state.title}
          placeholder="Adicione um Título"
          onChange={this.handleChangeTitleText}
        />

        <div className="new-task-description-container">
          <FaBars className="new-task-icons" />
          <AutoResizeTextArea
            className="new-task-textarea"
            minRows={1}
            maxRows={3}
            value={this.state.description}
            placeholder="Adicione detalhes..."
            onChange={this.handleChangeDescriptionText}
            style={{ alignSelf: 'stretch' }}
          />
        </div>

        {this.getDateDiv()}

        <hr />

        <div className="new-task-buttons-container">
          <div className="btn btn-blue" onClick={this.handleSaveTask}>
            {' '}
            Salvar{' '}
          </div>
          <div className="btn" onClick={this.props.handleCancelAddTask}>
            {' '}
            Cancelar{' '}
          </div>
        </div>
      </div>
    )
  }

  render() {
    return <Modal render={this.getInsideModalRender()} />
  }
}

export default NewTaskModal

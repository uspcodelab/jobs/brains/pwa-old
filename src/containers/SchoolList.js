import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import List from '../components/List'
import { setHeader } from '../actions/setHeader'
import { fetchSchools, selectSchool } from '../actions/schoolList'
import { FaPencil } from 'react-icons/lib/fa'

class SchoolList extends Component {
  constructor(props) {
    super(props)

    this.itemRender = this.itemRender.bind(this)
    this.schoolClick = this.schoolClick.bind(this)
  }

  componentDidMount() {
    if (!this.props.schoolList.length)
      this.props.fetchSchools(this.props.activeProfiles)
    this.props.setHeader(
      'Escolas',
      () => this.props.history.goBack(),
      () => this.props.history.push('/school_new')
    )
  }

  schoolClick(obj) {
    this.props.history.push('/classroom_list')
    this.props.selectSchool(obj)
  }

  schoolEdit(obj, ev) {
    ev.stopPropagation()
    this.props.selectSchool(obj)
    this.props.history.push('/school_edit')
  }

  itemRender(obj) {
    return (
      <div className="card" key={obj.id} onClick={() => this.schoolClick(obj)}>
        <div className="card-title">
          {obj.name}
          <FaPencil onClick={ev => this.schoolEdit(obj, ev)} />
        </div>
      </div>
    )
  }

  render() {
    return (
      <section className="page">

        <List
          objList={this.props.schoolList}
          itemRender={this.itemRender}
          buttonLink={'schools_new'}
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    fetchSchools,
    selectSchool,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    schoolList: state.schoolList,
    activeProfiles: state.activeProfiles
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SchoolList)

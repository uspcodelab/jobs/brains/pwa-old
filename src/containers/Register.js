import React, { Component } from 'react'
import gql from 'graphql-tag'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { client } from '..'
import { setHeader } from '../actions/setHeader'
import Form from '../components/Form'

class Register extends Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    this.props.setHeader(
      'Brains',
      () => this.props.history.goBack()
    )
  }

  async submit(args) {
    const register = gql`
      mutation registerFunc(
        $name: String!
        $email: String!
        $password: String!
      ) {
        signup(name: $name, email: $email, password: $password) {
          name
        }
      }
    `

    await client.mutate({
      mutation: register,
      variables: {
        name: args.name,
        email: args.email,
        password: args.password,
      },
    })

    this.props.history.push('/login')
  }

  render() {
    const inputs = [
      {
        name: 'Nome',
        input: {
          name: 'name',
          type: 'text',
        },
      },
      {
        name: 'Email',
        input: {
          name: 'email',
          type: 'email',
        },
      },
      {
        name: 'Senha',
        input: {
          name: 'password',
          type: 'password',
        },
      },
    ]

    const state = {
      name: '',
      email: '',
      password: '',
    }

    return (
      <section className="form-section">
        <Form
          title="Cadastro"
          state={state}
          inputs={inputs}
          submitHandler={this.submit}
          action="Cadastrar"
          question="Já possui conta?"
          link="/login"
          answer="Clique Aqui!"
        />
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

export default connect(null, mapDispatchToProps)(Register)

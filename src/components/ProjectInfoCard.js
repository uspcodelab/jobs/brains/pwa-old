import React, { Component } from 'react'
import { FaCaretRight, FaCaretDown, FaExclamation } from 'react-icons/lib/fa'
import { TiEdit } from 'react-icons/lib/ti'
import gql from 'graphql-tag'

import { client } from '../index'

async function submitUpdate(update, variables) {
  const response = await client.mutate({
    mutation: update,
    variables,
  })
}

class ProjectInfoCard extends Component {
  constructor(props) {
    super(props)

    this.textLastState = props.text
    this.state = {
      isOpen: false,
      editText: false,
      inputText: this.textLastState ? this.textLastState : '',
    }
  }

  getCardTitle() {
    const translate = {
      name: 'Nome',
      description: 'Descrição',
      objective: 'Objetivo',
      product: 'Produto',
      result: 'Resultados',
      marketing: 'Marketing',
      justification: 'Justificativa',
      methodology: 'Metodologia',
    }
    return translate[this.props.title]
  }

  toggleAccordion(ev) {
    this.setState({ isOpen: !this.state.isOpen })
  }

  cancelEdit(ev) {
    this.setState({
      editText: false,
      inputText: this.textLastState,
    })
  }

  submitEdit(ev) {
    this.textLastState = this.state.inputText
    const titleModified =
      this.props.title.charAt(0).toUpperCase() + this.props.title.substring(1)
    const changeField = gql`
      mutation changeField($projectId: ID!, $field: String!) {
        updateProject${titleModified}(id: $projectId, ${
      this.props.title
    }: $field) {
          id
          name
          ${this.props.title}
        }
      }
    `

    submitUpdate(changeField, {
      projectId: this.props.projectId,
      field: this.textLastState,
    })

    this.setState({
      editText: false,
    })
  }

  startEdit(ev) {
    console.log(ev)
    this.setState({
      isOpen: true,
      editText: true,
    })
  }

  changeInputText(ev) {
    this.setState({ inputText: ev.target.value })
  }

  getButton(text, callback, buttonClass) {
    buttonClass =
      buttonClass == null ? 'project-info-card-panel-button' : buttonClass
    return (
      <div className={buttonClass} onClick={callback}>
        {text}
      </div>
    )
  }

  getPanelObject() {
    const panelClasses =
      'project-info-card-panel' +
      (!this.state.isOpen ? ' project-info-card-panel-closed' : '')
    if (this.state.editText === false) {
      return (
        <div className={panelClasses}>
          <div className="project-info-card-text">{this.state.inputText}</div>
        </div>
      )
    } else {
      return (
        <div className={panelClasses}>
          <textarea
            className="project-info-card-panel-edit-mode-input"
            type="text"
            value={this.state.inputText}
            onChange={ev => this.changeInputText(ev)}
          />
          <div className="project-info-card-panel-edit-mode-buttons-container">
            {this.getButton(
              'Cancelar',
              ev => this.cancelEdit(ev),
              'project-info-card-panel-edit-mode-button'
            )}
            {this.getButton(
              'Salvar',
              ev => this.submitEdit(ev),
              'project-info-card-panel-edit-mode-button'
            )}
          </div>
        </div>
      )
    }
  }

  isEmpty() {
    return this.state.inputText.length === 0
  }

  getEmptyAlertObject() {
    if (this.isEmpty()) {
      return (
        <FaExclamation className="project-info-card-accordion-exclamation" />
      )
    } else return <div className="project-info-card-accordion-exclamation" />
  }

  getCaretIcon() {
    if (this.state.isOpen) {
      return <FaCaretDown />
    } else {
      return <FaCaretRight />
    }
  }

  getAccordionObject() {
    const accordionTitleClasses =
      'project-info-card-accordion-title' +
      (this.state.isOpen
        ? ' project-info-card-accordion-open'
        : ' project-info-card-accordion-closed')
    const accordionTitleColor = {
      color: this.state.inputText.length === 0 ? 'red' : null,
    }
    return (
      <div className="project-info-card-accordion" style={accordionTitleColor}>
        <div
          className={accordionTitleClasses}
          onClick={ev => this.toggleAccordion(ev)}
        >
          {this.getCaretIcon()}
          {this.getCardTitle()}
        </div>
        {this.getEmptyAlertObject()}
        <TiEdit
          className="project-info-card-accordion-title-edit"
          onClick={ev => this.startEdit(ev)}
        />
      </div>
    )
  }

  render() {
    return (
      <div className="project-info-card">
        {this.getAccordionObject()}
        {this.getPanelObject()}
      </div>
    )
  }
}

export default ProjectInfoCard

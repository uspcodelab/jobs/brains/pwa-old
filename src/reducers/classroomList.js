import {
  FETCH_CLASSROOMS,
  ADD_CLASSROOM,
  UPDATE_CLASSROOM,
  DELETE_CLASSROOM
} from '../actions/classroomList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_CLASSROOMS:
      return action.classroomList

    case ADD_CLASSROOM:
      return [...state, action.payload]

    case UPDATE_CLASSROOM:
      return state.map(classroom => {
        if (classroom.id === action.payload.id)
          return action.payload
        return classroom
      })

    case DELETE_CLASSROOM:
      let newState = [];
      state.forEach(classroom => {
        if(classroom.id !== action.payload.id)
          newState.push(classroom)
      })

      return newState

    default:
      return state
  }
}

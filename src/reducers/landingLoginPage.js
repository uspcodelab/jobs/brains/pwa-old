import { SET_HEADER_LANDING_LOGIN_PAGE } from '../actions/setHeader'

export default (state = '/', action) => {
  switch(action.type) {
    case SET_HEADER_LANDING_LOGIN_PAGE:
      return action.landingLoginPage

    default:
      return state
  }
}

export const SET_HEADER = 'SET_HEADER'
export const SET_HEADER_HEIGHT = 'SET_HEADER_HEIGHT'
export const SET_HEADER_LANDING_LOGIN_PAGE = 'SET_HEADER_LANDING_LOGIN_PAGE'

export const setHeader = (
  title = 'Brains',
  goBackCallback = null,
  addCallback = null
) => {
  return {
    type: SET_HEADER,
    payload: {
      title,
      goBackCallback,
      addCallback
    }
  }
}

export const setHeaderHeight = (height = 0) => {
  return {
    type: SET_HEADER_HEIGHT,
    height
  }
}

export const setHeaderLandingLoginPage = (landingLoginPage = null) => {
  return {
    type: SET_HEADER_LANDING_LOGIN_PAGE,
    landingLoginPage
  }
}
import gql from 'graphql-tag'

export const getAllSchools = gql`
  query getAllSchoolsQuery {
    getAllSchools {
      id
      name
    }
  }
`

export const getUserCoordination = gql`
  query getUserCoordinationQuery {
    getUserCoordination {
      id
      name
    }
  }
`

export const getUserAdvisingProjects = gql`
  query getUserAdvisingProjectsQuery {
    getUserAdvisingProjects {
      id
      name
    }
  }
`

export const getUserResponsableProjects = gql`
  query getUserResponsableProjectsFunc {
    getUserResponsableProjects {
      id
      name
    }
  }
`

export const getUserProjects = gql`
  query getUserProjectsQuery {
    getUserProjects {
      id
      name
    }
  }
`
export const getProjectsByClassroom = gql`
  query getProjectsByClassroomFunc($classroomId: ID!) {
    getProjectsByClassroom(classroomId: $classroomId) {
      id
      name
    }
  }
`

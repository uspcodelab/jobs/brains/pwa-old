import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'

import HeaderReducer from '../reducers/header'
import MenuReducer from '../reducers/menu'
import HeaderHeightReducer from '../reducers/headerHeight'
import HeaderLandingLoginPageReducer from '../reducers/landingLoginPage'

import ActiveProjectReducer from '../reducers/activeProject'
import ActiveClassReducer from '../reducers/activeClassroom'
import ActiveSchoolReducer from '../reducers/activeSchool'
import ActiveProfilesReducer from '../reducers/activeProfiles'

import ProjectListReducer from '../reducers/projectList'
import ProjectListStudentReducer from '../reducers/projectListStudent'
import ProjectListAdvisorReducer from '../reducers/projectListAdvisor'
import ProjectListResponsibleReducer from '../reducers/projectListResponsible'

import ClassroomListReducer from '../reducers/classroomList'
import SchoolListReducer from '../reducers/schoolList'

import SelectedProfileReducer from '../reducers/selectedProfile'

const parseProfiles = () => {
  const profiles = localStorage.getItem('profiles')

  if (!profiles)
    return []

  if (profiles.includes(','))
    return profiles.split(',')

  return ([profiles])
}

const initialState = {
  header: { title: 'Brains', goBackCallback: null, addCallback: null },
  headerHeight: 0,
  landingLoginPage: null,
  menu: false,
  activeProject: JSON.parse(localStorage.getItem('project')),
  activeClass: JSON.parse(localStorage.getItem('classroom')),
  activeSchool: JSON.parse(localStorage.getItem('school')),
  activeProfiles: parseProfiles(),
  projectList: [],
  projectListStudent: [],
  projectListAdvisor: [],
  projectListResponsible: [],
  classroomList: [],
  schoolList: [],
  selectedProfile: '',
}

const rootReducer = combineReducers({
  header: HeaderReducer,
  headerHeight: HeaderHeightReducer,
  landingLoginPage: HeaderLandingLoginPageReducer,
  menu: MenuReducer,
  activeProject: ActiveProjectReducer,
  activeClass: ActiveClassReducer,
  activeSchool: ActiveSchoolReducer,
  activeProfiles: ActiveProfilesReducer,
  projectList: ProjectListReducer,
  projectListStudent: ProjectListStudentReducer,
  projectListAdvisor: ProjectListAdvisorReducer,
  projectListResponsible: ProjectListResponsibleReducer,
  classroomList: ClassroomListReducer,
  schoolList: SchoolListReducer,
  selectedProfile: SelectedProfileReducer,
})

export default createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(ReduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

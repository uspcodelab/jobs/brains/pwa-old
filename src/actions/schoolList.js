import gql from 'graphql-tag'

import { client } from '..'
import { getAllSchools, getUserCoordination } from './queries'

export const FETCH_SCHOOLS = 'FETCH_SCHOOLS'
export const SELECT_SCHOOL = 'SELECT_SCHOOL'
export const CLEAN_SCHOOL = 'CLEAN_SCHOOL'
export const ADD_SCHOOL = 'ADD_SCHOOL'
export const UPDATE_SCHOOL = 'UPDATE_SCHOOL'
export const DELETE_SCHOOL = 'DELETE_SCHOOL'

export const fetchSchools = (profiles) => async dispatch => {
  const query = profiles.includes("MASTER")
    ? getAllSchools
    : getUserCoordination

  const response = await client.query({ query })

  dispatch({
    type: FETCH_SCHOOLS,
    schoolList: response.data.getAllSchools,
  })
}

export const selectSchool = school => {
  localStorage.setItem('school', JSON.stringify(school))

  return {
    type: SELECT_SCHOOL,
    school,
  }
}

export const cleanSchool = () => ({
  type: CLEAN_SCHOOL,
  payload: null
})

export const newSchool = ({ name }) => async dispatch => {
  const createSchool = gql`
    mutation createSchoolFunc($name: String!) {
      createSchool(name: $name) {
        id
        name
      }
    }
  `

  const response = await client.mutate({
    mutation: createSchool,
    variables: {
      name
    },
  })

  dispatch({
    type: ADD_SCHOOL,
    payload: response.data.createSchool
  })
}

export const updateSchool = (id, name) => async dispatch => {
  const updateSchool = gql`
    mutation updateSchoolFunc($id: ID!, $name: String!) {
      updateSchool(id: $id, name: $name) {
        id
        name
      }
    }
  `

  const response = await client.mutate({
    mutation: updateSchool,
    variables: {
      id,
      name
    }
  })

  dispatch({
    type: UPDATE_SCHOOL,
    payload: response.data.updateSchool
  })

  dispatch(selectSchool(response.data.updateSchool))
}

export const deleteSchool = id => async dispatch => {
  const deleteSchool = gql`
    mutation deleteSchoolFunc($id: ID!) {
      deleteSchool(id: $id) {
        id
      }
    }
  `

  const response = await client.mutate({
    mutation: deleteSchool,
    variables: {
      id
    }
  })

  dispatch({
    type: DELETE_SCHOOL,
    payload: response.data.deleteSchool
  })

  dispatch(cleanSchool())
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { setHeader } from '../actions/setHeader'

class MenuClass extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { year, complement, period } = this.props.activeClass
    const title = `${year}º ${complement} ${period}`

    setHeader(
      title,
      () => this.props.history.push('/project_list')
    )
  }

  render() {

    return (
      <div className="page">
        <div
          className="btn btn-yellow btn-link"
          onClick={() => this.props.history.push('/class_management')}
        >
          <div>Associar aluno a esta turma</div>
        </div>

        <div
          className="btn btn-yellow btn-link"
          onClick={() => this.props.history.push('/project_new')}
        >
          <div>Criar Projeto </div>
        </div>
      </div>
    )
  }
}


const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    activeClass: state.activeClass
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuClass)

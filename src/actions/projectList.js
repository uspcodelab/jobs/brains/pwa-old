import gql from 'graphql-tag';
import { client } from '..'

import {
  getUserProjects,
  getProjectsByClassroom,
  getUserAdvisingProjects,
  getUserResponsableProjects
} from './queries'

import {
  createProject,
  connectStudents,
  connectAdvisors,
  updtProject,
  delProject
} from './mutations'

export const FETCH_PROJECTS = 'FETCH_PROJECTS'
export const FETCH_PROJECTS_STUDENT = 'FETCH_PROJECTS_STUDENT'
export const FETCH_PROJECTS_ADVISOR = 'FETCH_PROJECTS_ADVISOR'
export const FETCH_PROJECTS_RESPONSIBLE = 'FETCH_PROJECTS_RESPONSIBLE'
export const SELECT_PROJECT = 'SELECT_PROJECT'
export const CLEAN_PROJECT = 'CLEAN_PROJECT'
export const ADD_PROJECT = 'ADD_PROJECT'
export const UPDATE_PROJECT = 'UPDATE_PROJECT'
export const DELETE_PROJECT = 'DELETE_PROJECT'

const fetchProjectsStudent = async () => {
  const response = await client.query({
    query: getUserProjects
  })

  return response.data.getUserProjects;
}

const fetchProjectsAdvisor = async () => {
  const response = await client.query({
    query: getUserAdvisingProjects
  })

  return response.data.getUserAdvisingProjects;
}

const fetchProjectsResponsible = async () => {
  const response = await client.query({
    query: getUserResponsableProjects
  })

  return response.data.getUserResponsableProjects;
}

export const fetchMultipleProjects = profiles => async dispatch => {
  let projectList

  if (profiles.includes('STUDENT')) {
    projectList = await fetchProjectsStudent()

    dispatch({
      type: FETCH_PROJECTS_STUDENT,
      projectList
    })
  }

  if (profiles.includes('ADVISOR')) {
    projectList = await fetchProjectsAdvisor()

    dispatch({
      type: FETCH_PROJECTS_ADVISOR,
      projectList
    })
  }

  if (profiles.includes('RESPONSIBLE')) {
    projectList = await fetchProjectsResponsible()

    dispatch({
      type: FETCH_PROJECTS_RESPONSIBLE,
      projectList
    })
  }
}

export const fetchProjects = (profiles, classroomId = null) => async dispatch => {
  let projectList

  if (!classroomId) {

    if (profiles.includes('STUDENT'))
      projectList = await fetchProjectsStudent()
    else if (profiles.includes('ADVISOR'))
      projectList = await fetchProjectsAdvisor()
    else
      projectList = await fetchProjectsResponsible()

  }

  else {
    const response = await client.query({
      query: getProjectsByClassroom,
      variables: {
        classroomId,
      },
    })

    projectList = response.data.getProjectsByClassroom
  }

  console.log(projectList)

  dispatch({
    type: FETCH_PROJECTS,
    projectList
  })
}

export const selectProject = project => {
  localStorage.setItem('project', JSON.stringify(project))

  return {
    type: SELECT_PROJECT,
    project,
  }
}

export const cleanProject = () => ({
  type: CLEAN_PROJECT,
  payload: null
})

export const newProject = args => async dispatch => {
  const responseProject = await client.mutate({
    mutation: createProject,
    variables: {
      name: args.name,
      objective: '',
      description: '',
      methodology: '',
      justification: '',
      marketing: '',
      product: '',
      result: '',
    },
  })

  let studentsEmails = []
  for (let i = 1; i < 5; i++)
    if (args[`student${i}`])
      studentsEmails.push(args[`student${i}`])

  let advisorsEmails = []
  for (let i = 1; i < 3; i++)
    if (args[`advisor${i}`])
      advisorsEmails.push(args[`advisor${i}`])

  await client.mutate({
    mutation: connectStudents,
    variables: {
      id: responseProject.data.createProject.id,
      emails: studentsEmails
    }
  })

  await client.mutate({
    mutation: connectAdvisors,
    variables: {
      id: responseProject.data.createProject.id,
      emails: advisorsEmails
    }
  })

  dispatch({
    type: ADD_PROJECT,
    payload: responseProject.data.createProject
  })
}

export const updateProject = args => async dispatch => {

  const response = await client.mutate({
    mutation: updtProject,
    variables: {
      id: args.id,
      name: args.name
    }
  })

  dispatch({
    type: UPDATE_PROJECT,
    payload: response.data.updateProject
  })

  dispatch(selectProject(response.data.updateProject))
}

export const deleteProject = id => async dispatch => {

  const response = await client.mutate({
    mutation: delProject,
    variables: {
      id
    }
  })

  dispatch({
    type: DELETE_PROJECT,
    payload: response.data.deleteProject
  })

  dispatch(cleanProject())
}
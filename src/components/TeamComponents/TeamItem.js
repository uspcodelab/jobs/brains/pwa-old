import React, { Component } from 'react';

class TeamItem extends Component {
  
  render() {
    return (
      <div className='team-item-container'>
        <div className='team-item-prop'> 
          { this.props.user.name }
        </div>
        <div className='team-item-prop'>
          { this.props.user.email }
        </div>
      </div> 
    )  
  }
}

export default TeamItem;
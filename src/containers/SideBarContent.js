import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import gql from 'graphql-tag'

import { cleanProfiles } from '../actions/setProfiles'
import { cleanSchool } from '../actions/schoolList'
import { cleanClass } from '../actions/classroomList'
import { cleanProject } from '../actions/projectList'
import { closeMenu } from '../actions/openMenu'

import { client } from '..'

export const setStyle = height => {
  height = `${height}px`

  return {
    root: {
      top: height
    },
    overlay: {
      top: height
    },
    sidebar: {
      background: '#f7f7fc',
      padding: '0 1em'
    },
    dragHandle: {
      top: height
    }
  }
}

const logout = gql`
  mutation logOutFunc {
    logout
  }
`

class SideBarContent extends Component {
  constructor(props) {
    super(props)

    this.haveProfiles = this.haveProfiles.bind(this)
    this.redirectAndClose = this.redirectAndClose.bind(this)
    this.logOut = this.logOut.bind(this)

    this.renderMySchools = this.renderMySchools.bind(this)
    this.renderMyClasses = this.renderMyClasses.bind(this)
    this.renderMyProjects = this.renderMyProjects.bind(this)

    this.renderUserManagement = this.renderUserManagement.bind(this)
    this.renderSchoolManagement = this.renderSchoolManagement.bind(this)
    this.renderClassManagement = this.renderClassManagement.bind(this)

    this.renderSchoolCreate = this.renderSchoolCreate.bind(this)
    this.renderClassCreate = this.renderClassCreate.bind(this)
    this.renderProjectCreate = this.renderProjectCreate.bind(this)

    this.renderLastSpacer = this.renderLastSpacer.bind(this)

    this.lastSapcer = false
  }

  redirectAndClose(to) {
    this.props.closeMenu()
    this.props.redirect(to)
  }

  async logOut() {
    await client.mutate({
      mutation: logout
    })

    localStorage.clear()
    this.redirectAndClose('/')

    this.props.cleanSchool()
    this.props.cleanClass()
    this.props.cleanProject()
    this.props.cleanProfiles()

  }

  haveProfiles(profiles) {
    const { activeProfiles } = this.props
    if (!activeProfiles)
      return false

    return profiles.some(profile => {
      return activeProfiles.some(aProfile =>  profile === aProfile)
    })
  }

  renderLogOut() {
    if (!localStorage.getItem('token'))
      return (
        <Fragment>
          <div
            onClick={() => this.redirectAndClose('/login')}
            className="sidebar-item"
          >
            Login
          </div>
          <div
            onClick={() => this.redirectAndClose('/register')}
            className="sidebar-item"
          >
            Register
          </div>
        </Fragment>
      )

    return (
      <div
        onClick={ this.logOut }
        className="sidebar-item"
      >
        LogOut
      </div>
    )
  }

  renderMySchools() {
    if (!this.haveProfiles(['MASTER', 'TEACHER']))
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('school_list')}
        className="sidebar-item"
      >
        Escolas
      </div>
    )
  }

  renderMyClasses() {
    if (!this.haveProfiles(['MASTER', 'TEACHER']) || !this.props.activeSchool)
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('/classroom_list')}
        className="sidebar-item"
      >
        Turmas
      </div>
    )
  }

  renderMyProjects() {
    if (
      (!this.haveProfiles(['MASTER', 'TEACHER']) || !this.props.activeClass) &&
      (!this.haveProfiles(['STUDENT', 'ADVISOR', 'RESPONSIBLE'])))
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('/project_list')}
        className="sidebar-item"
      >
        Projetos
      </div>
    )
  }

  renderSchoolCreate() {
    if (!this.haveProfiles(['MASTER']))
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('/school_new')}
        className="sidebar-item"
      >
        Criar Escola
      </div>
    )
  }

  renderClassCreate() {
    if (!this.haveProfiles(['MASTER', 'TEACHER']) || !this.props.activeSchool)
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('/classroom_new')}
        className="sidebar-item"
      >
        Criar Turma
      </div>
    )
  }

  renderProjectCreate() {
    if(!this.haveProfiles(['MASTER', 'TEACHER', 'ADVISOR', 'STUDENT']))
      return null

    return (
      <div
        onClick={() => this.redirectAndClose('/project_new')}
        className="sidebar-item"
      >
        Criar Projeto
      </div>
    )
  }

  renderUserManagement() {
    if (!this.haveProfiles(['MASTER']))
      return null

    this.lastSapcer = true
    return (
      <div
        onClick={() => this.redirectAndClose('/profile_management')}
        className="sidebar-item"
      >
        Gerenciar Usuários
      </div>
    )
  }

  renderSchoolManagement() {
    if (!this.haveProfiles(['MASTER']) || !this.props.activeSchool)
      return null

    this.lastSapcer = true

    return (
      <div className="sidebar-item">
        Gerenciar Escola
      </div>
    )
  }

  renderClassManagement() {
    if (!this.haveProfiles(['MASTER', 'TEACHER']) || !this.props.activeClass)
      return null

    this.lastSapcer = true

    return (
      <div
        onClick={() => this.redirectAndClose('/class_management')}
        className="sidebar-item"
      >
        Gerenciar Turma
      </div>
    )
  }

  renderLastSpacer() {
    if (!this.lastSapcer)
      return null

    return <div className="sidebar-spacer"/>
  }

  render() {
    return (
      <div>
        <div className='sidebar-title'>
          SideBar
        </div>
        <div className="sidebar-spacer"/>
        { this.renderMySchools() }
        { this.renderMyClasses() }
        { this.renderMyProjects() }
        <div className="sidebar-spacer"/>
        { this.renderSchoolCreate() }
        { this.renderClassCreate() }
        { this.renderProjectCreate() }
        <div className="sidebar-spacer"/>
        { this.renderUserManagement() }
        { this.renderSchoolManagement() }
        { this.renderClassManagement() }
        { this.renderLastSpacer() }
        { this.renderLogOut() }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    closeMenu,
    cleanClass,
    cleanSchool,
    cleanProfiles,
    cleanProject
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => ({
  activeProfiles: state.activeProfiles,
  activeSchool: state.activeSchool,
  activeClass: state.activeClass,
  headerHeight: state.headerHeight
})

export default connect(mapStateToProps, mapDispatchToProps)(SideBarContent)

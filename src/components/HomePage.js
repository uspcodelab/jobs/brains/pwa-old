import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { setHeader, setHeaderHeight } from '../actions/setHeader'
import Button from '../components/Button'
import WhiteLogo from '../images/logo-white.png'

class Home extends Component {
  constructor(props) {
    super(props)
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  componentDidMount() {
    this.props.setHeader('')
    this.props.setHeaderHeight(0)
  }

  handleButtonClick(url) {
    this.props.setHeaderHeight(51);
    this.props.history.push(url)
  }

  render() {
    return (
      <section className="home">
        <div className='title'>
          <img src={WhiteLogo} />
          <h1>Plataforma<br/>Brains</h1>
        </div>

        <div className='btn-wrap'>
          <Button class='btn-yellow btn-link' onClick={() => this.handleButtonClick('/login')} text='Login' />
          <Button class='btn-yellow btn-link' onClick={() => this.handleButtonClick('/register')} text='Cadastro' />
        </div>

        <div className='bg-overlay'></div>
      </section>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    setHeader,
    setHeaderHeight
  }

  return bindActionCreators(actions, dispatch)
}

export default connect(null, mapDispatchToProps)(Home)

import {
  FETCH_SCHOOLS,
  ADD_SCHOOL,
  UPDATE_SCHOOL,
  DELETE_SCHOOL
} from '../actions/schoolList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_SCHOOLS:
      return action.schoolList

    case ADD_SCHOOL:
      return [...state, action.payload]

    case UPDATE_SCHOOL:
      return state.map(school => {
        if(school.id === action.payload.id)
          return action.payload
        return school
      })

    case DELETE_SCHOOL:
      let newState = [];
      state.forEach(school => {
        if (school.id !== action.payload.id)
          newState.push(school)
      })

      return newState

    default:
      return state
  }
}

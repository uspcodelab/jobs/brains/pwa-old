import React, { Component } from 'react';

import TeamItem from './TeamItem';

class TeamList extends Component {

  constructor(props) {
    super(props);
    console.log(props);
  }

  getTeamList() {
    return this.props.members.map((member) => {
      return <TeamItem key={member.id} user={member}/>
    });
  }

  render() {
    return (
      <div className="team-list">
        { this.getTeamList() }
      </div>
    );
  }
}

export default TeamList;
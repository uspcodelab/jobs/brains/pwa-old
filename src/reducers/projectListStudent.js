import { FETCH_PROJECTS_STUDENT } from '../actions/projectList'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_PROJECTS_STUDENT:
      return action.projectList

    default:
      return state
  }
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import List from '../components/List'
import { setHeader } from '../actions/setHeader'
import { fetchSchools, selectSchool } from '../actions/schoolList'
import { FaPencil } from 'react-icons/lib/fa'
import Block from '../components/Block';

class SchoolList extends Component {
  constructor(props) {
    super(props)

    this.schoolClick = this.schoolClick.bind(this)
  }

  componentDidMount() {
    if (!this.props.schoolList.length)
      this.props.fetchSchools(this.props.activeProfiles)

    this.props.setHeader(
      'Escolas',
      () => this.props.history.goBack(),
      () => this.props.history.push('/school_new')
    )
  }

  schoolClick(obj) {
    this.props.history.push('/classroom_list')
    this.props.selectSchool(obj)
  }

  schoolEdit(obj, ev) {
    ev.stopPropagation()
    this.props.history.push('/school_edit')
    this.props.selectSchool(obj)
  }

  render() {
    
    return (
      <div className="block-list">
        { this.props.schoolList.map( (school) => {
          return (
            <div className="block-container" key={school.id} onClick={ () => this.schoolClick(school) }>
              <Block title={ school.name } edit="true" />
              <FaPencil className="block-icon" onClick={ev => this.schoolEdit(school, ev)} />
            </div>
          )
        }) }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  const actions = {
    fetchSchools,
    selectSchool,
    setHeader
  }

  return bindActionCreators(actions, dispatch)
}

const mapStateToProps = state => {
  return {
    schoolList: state.schoolList,
    activeProfiles: state.activeProfiles
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SchoolList)

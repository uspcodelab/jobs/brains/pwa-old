import React from 'react'

export default function(props) {
  return (
    <div className="progress-bar-border">
      <div className="progress-bar" style={{ width: props.progress + '%' }} />
      <div
        className={
          props.progress >= 45
            ? props.progress < 60
              ? 'yellow'
              : 'white'
            : 'blue'
        }
      >
        {props.progress + '%'}
      </div>
    </div>
  )
}
